﻿using Newtonsoft.Json;
using ControllerModelManager.Attributes;
using ControllerModelManager.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;
using ImageMapper_Module;
using OntoMsg_Module;
using System.Runtime.InteropServices;
using OntologyAppDBConnector;
using ControllerModelManager.Services;
using ControllerModelManager.BusinessFactories;
using ControllerModelManager.Translations;
using ControllerModelManager.BusinessModels;
using System.Xml;
using OntoMsg_Module.WebSocketServices;
using OntoMsg_Module.Services;
using OntoMsg_Module.Base;
using OntoMsg_Module.Notifications;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.StateMachines;
using System.ComponentModel;

namespace ControllerModelManager.Controllers
{
    public class ControllerModelManager_ViewModelController : ControllerModelManager_ViewModel, IViewController
    {
        
        private ImageMapperEngine imageMapperEngine;
        private clsLocalConfig localConfig;
        private TranslationController translationController = new TranslationController();

        private clsOntologyItem oItemUser;
        private clsOntologyItem oItemGroup;

        private OntoServiceAgent_ViewModelTree serviceAgentOnto_ViewModelTree;
        private OntoServiceAgent_ViewItem serviceAgentOnto_ViewItem;
        private ControllerModelTreeFactory viewModelTreeFactory;
        private ViewItemFactory viewItemFactory;

        private ImageItem rootImageItem;
        private ImageItem projectImageItem;
        private ImageItem developmentImageItem;
        private ImageItem viewModelImageItem;

        private List<ControllerModelTreeNode> treeModel;

        private WebsocketServiceAgent webSocketServiceAgent;

        public IControllerStateMachine StateMachine { get; private set; }
        public ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (ControllerStateMachine)StateMachine;
            }
        }

        public ControllerModelManager_ViewModelController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            


            Initialize();
            PropertyChanged += ControllerModelManager_ViewModelController_PropertyChanged;


        }

        
        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {
                var result = localConfig.Globals.LState_Success.Clone();
                var sessionFile = webSocketServiceAgent.RequestWriteStream(Notifications.NotifyChanges.ViewController_Resource_TreeData);
                if (sessionFile.FileUri == null)
                {
                    Message_FormMessage = new ViewMessage(translationController.Text_WebConfigError, translationController.Title_WebConfigError, ViewMessageType.Critical | ViewMessageType.OK);
                    IsActive_LoadProgressState = false;
                    return;
                }

                if (sessionFile.StreamWriter == null)
                {
                    Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                    IsActive_LoadProgressState = false;
                    return;
                }

                using (sessionFile.StreamWriter)
                {
                    result = viewModelTreeFactory.WriteXMLDoc(sessionFile.StreamWriter);

                }
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                    IsActive_LoadProgressState = false;
                    return;
                }

                

                Url_TreeData = sessionFile.FileUri.AbsoluteUri;
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.ViewController_Command_SendTreeModel)
                {
                    
                    IsActive_LoadProgressState = true;
                    RaisePropertyChanged(Notifications.NotifyChanges.View_Url_TreeData);
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.ViewController_Command_ChangeSelection)
                {
                    IsActive_LoadProgressState = true;
                    if (!webSocketServiceAgent.Request.ContainsKey(Notifications.NotifyChanges.ViewController_Parameter_id))
                    {
                        Message_FormMessage = new ViewMessage(string.Format(translationController.Text_ParamMissingError, Notifications.NotifyChanges.ViewController_Parameter_id), translationController.Title_ParamMissingError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }
                    var id = webSocketServiceAgent.Request["id"].ToString();
                    var selectedNode = treeModel.FirstOrDefault(nodeItem => nodeItem.Id == id);
                    if (selectedNode.IdNodeImage != viewModelImageItem.IdOImage)
                    {
                        Message_FormMessage = new ViewMessage(string.Format(translationController.Text_NodeNotPresentError, id), translationController.Title_NodeNotPresentError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }

                    var result = serviceAgentOnto_ViewItem.GetDataViewItems(id);
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_ModelItemLoadError, translationController.Title_ModelItemLoadError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;

                    }


                    var viewItemList = viewItemFactory.GetViewItems(serviceAgentOnto_ViewItem.ModelItems,
                        serviceAgentOnto_ViewItem.ModelItemTypesOfModelItems,
                        serviceAgentOnto_ViewItem.DataTypesOfModelItemTypes,
                        serviceAgentOnto_ViewItem.NotificationsOfModelItems);

                    if (viewItemList == null)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_ModelItemLoadError, translationController.Title_ModelItemLoadError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }

                    var sessionFile = webSocketServiceAgent.RequestWriteStream(Notifications.NotifyChanges.ViewController_Resource_ItemGridData);
                    if (sessionFile.FileUri == null)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_WebConfigError, translationController.Title_WebConfigError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }

                    if (sessionFile.StreamWriter == null)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }

                    using (sessionFile.StreamWriter)
                    {
                        result = viewItemFactory.WriteXMLDoc(sessionFile.StreamWriter);

                    }
                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                        return;
                    }


                    if (result.GUID == localConfig.Globals.LState_Error.GUID)
                    {
                        Message_FormMessage = new ViewMessage(translationController.Text_FileStreamError, translationController.Title_FileStreamError, ViewMessageType.Critical | ViewMessageType.OK);
                        IsActive_LoadProgressState = false;
                    }


                    Url_ItemGridData = sessionFile.FileUri.AbsoluteUri;
                }
                else if (webSocketServiceAgent.Command_RequestedCommand == Notifications.NotifyChanges.ViewController_Command_ResetProgressState)
                {
                    IsActive_LoadProgressState = false;
                }
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {
                if (webSocketServiceAgent.Command_RequestEvent == Notifications.NotifyChanges.ViewController_Event_ItemGridData_Loaded)
                {
                    webSocketServiceAgent.RemoveResource(Notifications.NotifyChanges.ViewController_Resource_ItemGridData);
                    IsActive_LoadProgressState = false;
                }
                if (webSocketServiceAgent.Command_RequestEvent == Notifications.NotifyChanges.ViewController_Event_TreeData_Loaded)
                {
                    webSocketServiceAgent.RemoveResource(Notifications.NotifyChanges.ViewController_Resource_TreeData);
                    IsActive_LoadProgressState = false;
                }
            }
        }

      

        private void Initialize()
        {
            var stateMachine = new ControllerStateMachine(StateMachineType.BlockSelectingSelected | StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            Text_View = "ControllerModel-Manager";
            DataText_ImagePath = "Images/";
            imageMapperEngine = new ImageMapperEngine(localConfig.Globals);
            var result = imageMapperEngine.GetImagesOfImageList(localConfig.OItem_object_ControllerModelManager);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                Message_FormMessage = new ViewMessage(translationController.Text_ImageListLoadError, translationController.Title_ImageListLoadError, ViewMessageType.Critical | ViewMessageType.OK);
                IsActive_LoadProgressState = false;
                return;
            }

            while (!imageMapperEngine.ListLoaded) { }

            rootImageItem = imageMapperEngine.ImageItemList.FirstOrDefault(imageItem => imageItem.IsRoot);
            projectImageItem = imageMapperEngine.ImageItemList.FirstOrDefault(imageItem => imageItem.IdReferenceItem == localConfig.OItem_class_software_project.GUID);
            developmentImageItem = imageMapperEngine.ImageItemList.FirstOrDefault(ImageItem => ImageItem.IdReferenceItem == localConfig.OItem_class_software_development.GUID);
            viewModelImageItem = imageMapperEngine.ImageItemList.FirstOrDefault(ImageItem => ImageItem.IdReferenceItem == localConfig.OItem_class_view_model.GUID);

            if (rootImageItem == null ||
                projectImageItem == null ||
                developmentImageItem == null ||
                viewModelImageItem == null )
            {
                Message_FormMessage = new ViewMessage(translationController.Text_ImageListTooShortError, translationController.Title_ImageListTooShortError, ViewMessageType.Critical | ViewMessageType.OK);
                IsActive_LoadProgressState = false;
                return;
            }
            serviceAgentOnto_ViewItem = new OntoServiceAgent_ViewItem(localConfig);
            serviceAgentOnto_ViewModelTree = new OntoServiceAgent_ViewModelTree(localConfig);
            
            result = serviceAgentOnto_ViewModelTree.GetDataViewModelTree();

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                Message_FormMessage = new ViewMessage(translationController.Text_ControllerModelTreeLoadError, translationController.Title_ControllerModelTreeLoadError, ViewMessageType.Critical | ViewMessageType.OK);
                IsActive_LoadProgressState = false;
                return;
            }
            viewItemFactory = new ViewItemFactory(localConfig, translationController);
            viewModelTreeFactory = new ControllerModelTreeFactory(serviceAgentOnto_ViewModelTree.ViewModelList,
                serviceAgentOnto_ViewModelTree.ViewModelsToDevelopments,
                serviceAgentOnto_ViewModelTree.ProjectsToDevelopments,
                rootImageItem,
                projectImageItem,
                developmentImageItem,
                viewModelImageItem,
                localConfig);


            treeModel = viewModelTreeFactory.GetViewModelTree();

            if (treeModel == null)
            {
                Message_FormMessage = new ViewMessage(translationController.Text_ControllerModelTreeLoadError, translationController.Title_ControllerModelTreeLoadError, ViewMessageType.Critical | ViewMessageType.OK);
                IsActive_LoadProgressState = false;
                return;
            }

            var values = viewItemFactory.GetHeaders();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_GridHeader = values;
            }

            values = viewItemFactory.GetColAligns();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_ColAligns = values;
            }

            values = viewItemFactory.GetColSorts();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_ColSorting = values;
            }

            values = viewItemFactory.GetColTypes();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_ColTypes = values;
            }

            values = viewItemFactory.GetInitWiths();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_InitWiths = values;
            }

            values = viewItemFactory.GetFilters();
            if (!string.IsNullOrEmpty(values))
            {
                DataText_Filter = values;
            }

            var intValues = viewItemFactory.GetHidden();
            if (intValues != null && intValues.Any())
            {
                Idlist_HiddenColumns = intValues;
            }

            Text_ViewItemTree = translationController.Text_ViewItemTree;
            Text_Details = translationController.Text_Details;
            Text_ModelItemCreation = translationController.Text_ModelItemCreation;
            Text_ModelItemList = translationController.Text_ModelItemList;
            Text_TreeNodeCreation = translationController.Text_TreeNodeCreation;

            IsEnabled_ItemName = false;
            Label_ItemType = translationController.Label_ItemType;
            Label_ItemName = translationController.Label_ItemName;

            Message_ConnectionLost = new ViewMessage(translationController.Text_ConnectionLost, translationController.Title_ConnectionLost, ViewMessageType.OK | ViewMessageType.Critical);
            
        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
            serviceAgentOnto_ViewItem = null;
            serviceAgentOnto_ViewModelTree = null;
            viewItemFactory = null;
            viewModelTreeFactory = null;
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

        }

        private void StateMachine_loginSucceded()
        {
            webSocketServiceAgent.SendModel();
           
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void ControllerModelManager_ViewModelController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;
            

        }


        private void WebSocketServiceAgent_comServerOpened()
        {
            var message = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(message);
        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {
            if (message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            
        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
