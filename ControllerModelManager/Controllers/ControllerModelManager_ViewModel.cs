﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;
using ControllerModelManager.Attributes;
using ControllerModelManager.Notifications;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;

namespace ControllerModelManager.Controllers
{

    public class ControllerModelManager_ViewModel : OntoMsg_Module.Base.ViewModelBase
    {

        private bool issuccessful_Login;
        [ViewModel(Send = true)]
        public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.Navigation_IsSuccessful_Login);

            }
        }

        private string text_View;
        [ViewModel(Send = true)]
        public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.ControllerModelManager_Text_View);

            }
        }

        private string datatext_ImagePath;
        [ViewModel(Send = true)]
        public string DataText_ImagePath
        {
            get { return datatext_ImagePath; }
            set
            {
                if (datatext_ImagePath == value) return;

                datatext_ImagePath = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_ImagePath);

            }
        }

        private string datatext_GridHeader;
        [ViewModel(Send = true)]
        public string DataText_GridHeader
        {
            get { return datatext_GridHeader; }
            set
            {
                if (datatext_GridHeader == value) return;

                datatext_GridHeader = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_GridHeader);

            }
        }

        private string datatext_InitWiths;
        [ViewModel(Send = true)]
        public string DataText_InitWiths
        {
            get { return datatext_InitWiths; }
            set
            {
                if (datatext_InitWiths == value) return;

                datatext_InitWiths = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_InitWiths);

            }
        }

        private string datatext_ColAligns;
        [ViewModel(Send = true)]
        public string DataText_ColAligns
        {
            get { return datatext_ColAligns; }
            set
            {
                if (datatext_ColAligns == value) return;

                datatext_ColAligns = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_ColAligns);

            }
        }

        private string datatext_ColTypes;
        [ViewModel(Send = true)]
        public string DataText_ColTypes
        {
            get { return datatext_ColTypes; }
            set
            {
                if (datatext_ColTypes == value) return;

                datatext_ColTypes = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_ColTypes);

            }
        }

        private string datatext_ColSorting;
        [ViewModel(Send = true)]
        public string DataText_ColSorting
        {
            get { return datatext_ColSorting; }
            set
            {
                if (datatext_ColSorting == value) return;

                datatext_ColSorting = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_ColSorting);

            }
        }

        private ViewMessage message_FormMessage;
        [ViewModel(Send = true)]
        public ViewMessage Message_FormMessage
        {
            get { return message_FormMessage; }
            set
            {
                if (message_FormMessage == value) return;

                message_FormMessage = value;

                RaisePropertyChanged(NotifyChanges.View_Message_FormMessage);

            }
        }

        private string url_TreeData;
        [ViewModel(Send = true)]
        public string Url_TreeData
        {
            get { return url_TreeData; }
            set
            {
                if (url_TreeData == value) return;

                url_TreeData = value;

                RaisePropertyChanged(NotifyChanges.View_Url_TreeData);

            }
        }

        private string url_ItemGridData;
        [ViewModel(Send = true)]
        public string Url_ItemGridData
        {
            get { return url_ItemGridData; }
            set
            {

                url_ItemGridData = value;

                RaisePropertyChanged(NotifyChanges.View_Url_ItemGridData);

            }
        }

        private List<int> idlist_HiddenColumns;
        [ViewModel(Send = true)]
        public List<int> Idlist_HiddenColumns
        {
            get { return idlist_HiddenColumns; }
            set
            {
                if (idlist_HiddenColumns == value) return;

                idlist_HiddenColumns = value;

                RaisePropertyChanged(NotifyChanges.View_IdList_HiddenColumns);

            }
        }

        private bool isactive_LoadProgressState;
        [ViewModel(Send = true)]
        public bool IsActive_LoadProgressState
        {
            get { return isactive_LoadProgressState; }
            set
            {
                if (isactive_LoadProgressState == value) return;

                isactive_LoadProgressState = value;

                RaisePropertyChanged(NotifyChanges.View_IsActive_LoadProgressState);

            }
        }

        private string datatext_Filter;
        [ViewModel(Send = true)]
        public string DataText_Filter
        {
            get { return datatext_Filter; }
            set
            {
                if (datatext_Filter == value) return;

                datatext_Filter = value;

                RaisePropertyChanged(NotifyChanges.View_DataText_Filter);

            }
        }

        private ViewMessage message_ConnectionLost;
        [ViewModel(Send = true)]
        public ViewMessage Message_ConnectionLost
        {
            get { return message_ConnectionLost; }
            set
            {
                if (message_ConnectionLost == value) return;

                message_ConnectionLost = value;

                RaisePropertyChanged(NotifyChanges.View_Message_ConnectionLost);

            }
        }

        private string text_ViewItemTree;
        [ViewModel(Send = true)]
        public string Text_ViewItemTree
        {
            get { return text_ViewItemTree; }
            set
            {
                if (text_ViewItemTree == value) return;

                text_ViewItemTree = value;

                RaisePropertyChanged(NotifyChanges.View_Text_ViewItemTree);

            }
        }

        private string text_Details;
        [ViewModel(Send = true)]
        public string Text_Details
        {
            get { return text_Details; }
            set
            {
                if (text_Details == value) return;

                text_Details = value;

                RaisePropertyChanged(NotifyChanges.View_Text_Details);

            }
        }

        private string text_ModelItemList;
        [ViewModel(Send = true)]
        public string Text_ModelItemList
        {
            get { return text_ModelItemList; }
            set
            {
                if (text_ModelItemList == value) return;

                text_ModelItemList = value;

                RaisePropertyChanged(NotifyChanges.View_Text_ModelItemList);

            }
        }

        private string text_TreeNodeCreation;
        [ViewModel(Send = true)]
        public string Text_TreeNodeCreation
        {
            get { return text_TreeNodeCreation; }
            set
            {
                if (text_TreeNodeCreation == value) return;

                text_TreeNodeCreation = value;

                RaisePropertyChanged(NotifyChanges.View_Text_TreeNodeCreation);

            }
        }

        private string text_ModelItemCreation;
        [ViewModel(Send = true)]
        public string Text_ModelItemCreation
        {
            get { return text_ModelItemCreation; }
            set
            {
                if (text_ModelItemCreation == value) return;

                text_ModelItemCreation = value;

                RaisePropertyChanged(NotifyChanges.View_Text_ModelItemCreation);

            }
        }

        private bool isenabled_ItemName;
        [ViewModel(Send = true)]
        public bool IsEnabled_ItemName
        {
            get { return isenabled_ItemName; }
            set
            {
                if (isenabled_ItemName == value) return;

                isenabled_ItemName = value;

                RaisePropertyChanged(NotifyChanges.View_IsEnabled_ItemName);

            }
        }

        private bool isvisble_ItemName;
        [ViewModel(Send = true)]
        public bool IsVisble_ItemName
        {
            get { return isvisble_ItemName; }
            set
            {
                if (isvisble_ItemName == value) return;

                isvisble_ItemName = value;

                RaisePropertyChanged(NotifyChanges.View_IsVisble_ItemName);

            }
        }

        private string text_ItemName;
        [ViewModel(Send = true)]
        public string Text_ItemName
        {
            get { return text_ItemName; }
            set
            {
                if (text_ItemName == value) return;

                text_ItemName = value;

                RaisePropertyChanged(NotifyChanges.View_Text_ItemName);

            }
        }

        private string label_ItemName;
        [ViewModel(Send = true)]
        public string Label_ItemName
        {
            get { return label_ItemName; }
            set
            {
                if (label_ItemName == value) return;

                label_ItemName = value;

                RaisePropertyChanged(NotifyChanges.View_Label_ItemName);

            }
        }

        private bool isvisble_ItemType;
        [ViewModel(Send = true)]
        public bool IsVisble_ItemType
        {
            get { return isvisble_ItemType; }
            set
            {
                if (isvisble_ItemType == value) return;

                isvisble_ItemType = value;

                RaisePropertyChanged(NotifyChanges.View_IsVisble_ItemType);

            }
        }

        private string label_ItemType;
        [ViewModel(Send = true)]
        public string Label_ItemType
        {
            get { return label_ItemType; }
            set
            {
                if (label_ItemType == value) return;

                label_ItemType = value;

                RaisePropertyChanged(NotifyChanges.View_Label_ItemType);

            }
        }

        private bool isenabled_ItemTypes;
        [ViewModel(Send = true)]
        public bool IsEnabled_ItemTypes
        {
            get { return isenabled_ItemTypes; }
            set
            {
                if (isenabled_ItemTypes == value) return;

                isenabled_ItemTypes = value;

                RaisePropertyChanged(NotifyChanges.View_IsEnabled_ItemTypes);

            }
        }

        private bool isvisble_ItemTypes;
        [ViewModel(Send = true)]
        public bool IsVisble_ItemTypes
        {
            get { return isvisble_ItemTypes; }
            set
            {
                if (isvisble_ItemTypes == value) return;

                isvisble_ItemTypes = value;

                RaisePropertyChanged(NotifyChanges.View_IsVisble_ItemTypes);

            }
        }

        private List<clsOntologyItem> oitemlist_ItemTypes;
        [ViewModel(Send = true)]
        public List<clsOntologyItem> OItemList_ItemTypes
        {
            get { return oitemlist_ItemTypes; }
            set
            {
                if (oitemlist_ItemTypes == value) return;

                oitemlist_ItemTypes = value;

                RaisePropertyChanged(NotifyChanges.View_OItemList_ItemTypes);

            }
        }

        private string id_SelectedTab;
        [ViewModel(Send = true)]
        public string Id_SelectedTab
        {
            get { return id_SelectedTab; }
            set
            {
                if (id_SelectedTab == value) return;

                id_SelectedTab = value;

                RaisePropertyChanged(NotifyChanges.View_Id_SelectedTab);

            }
        }

    }
}