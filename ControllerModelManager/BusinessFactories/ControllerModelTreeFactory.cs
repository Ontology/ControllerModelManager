﻿using ImageMapper_Module;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControllerModelManager.BusinessModels;
using System.Xml;
using System.IO;

namespace ControllerModelManager.BusinessFactories
{
    public class ControllerModelTreeFactory
    {

        private List<clsOntologyItem> controllerModelItems;
        private List<clsObjectRel> controllerModelsToDevelopments;
        private List<clsObjectRel> projectsToDevelopments;

        private ImageItem rootImageItem;
        private ImageItem projectImageItem;
        private ImageItem developmentImageItem;
        private ImageItem controllerModelImageItem;

        private clsLocalConfig localConfig;

        private List<ControllerModelTreeNode> nodeList;

        public List<ControllerModelTreeNode> GetViewModelTree()
        {
            nodeList = new List<ControllerModelTreeNode>();
            var rootNode = new ControllerModelTreeNode
            {
                Id = localConfig.Globals.Root.GUID,
                Name = localConfig.Globals.Root.Name,
                IdNodeImage = rootImageItem.IdOImage,
                NameNodeImage = rootImageItem.IdOImage,
                IdPath = localConfig.Globals.Root.GUID
            };
            nodeList.Add(rootNode);

            var result = GetSubNodes(rootNode);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                return nodeList;
            }
            else
            {
                return null;
            }
        }

        private clsOntologyItem GetSubNodes(ControllerModelTreeNode parentNode)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var subNodes = new List<ControllerModelTreeNode>();
            if (parentNode.IdNodeImage == rootImageItem.IdOImage)
            {
                subNodes = projectsToDevelopments.GroupBy(projToDev => new
                {
                    Id = projToDev.ID_Object,
                    Name = projToDev.Name_Object
                }).Select(projToDev => new ControllerModelTreeNode
                {
                    ParentNode = parentNode,
                    Id = projToDev.Key.Id,
                    Name = projToDev.Key.Name,
                    IdNodeImage = projectImageItem.IdOImage,
                    NameNodeImage = projectImageItem.IdOImage,
                    IdPath = parentNode.IdPath + projToDev.Key.Name
                }).ToList();
                

            }
            else if (parentNode.IdNodeImage == projectImageItem.IdOImage)
            {
                subNodes = projectsToDevelopments.Where(projToDev => projToDev.ID_Object == parentNode.Id).GroupBy(projToDev => new
                {
                    Id = projToDev.ID_Other,
                    Name = projToDev.Name_Other
                }).Select(projToDev => new ControllerModelTreeNode
                {
                    ParentNode = parentNode,
                    Id = projToDev.Key.Id,
                    Name = projToDev.Key.Name,
                    IdNodeImage = developmentImageItem.IdOImage,
                    NameNodeImage = developmentImageItem.IdOImage,
                    IdPath = parentNode.IdPath + projToDev.Key.Id
                }).ToList();
            }
            else if (parentNode.IdNodeImage == developmentImageItem.IdOImage)
            {
                subNodes = controllerModelsToDevelopments.Where(projToDev => projToDev.ID_Other == parentNode.Id).GroupBy(projToDev => new
                {
                    Id = projToDev.ID_Object,
                    Name = projToDev.Name_Object
                }).Select(projToDev => new ControllerModelTreeNode
                {
                    ParentNode = parentNode,
                    Id = projToDev.Key.Id,
                    Name = projToDev.Key.Name,
                    IdNodeImage = controllerModelImageItem.IdOImage,
                    NameNodeImage = controllerModelImageItem.IdOImage,
                    IdPath = parentNode.IdPath + projToDev.Key.Id
                }).ToList();
            }

            parentNode.SubNodes = subNodes;
            nodeList.AddRange(subNodes);
            foreach (var subNode in subNodes)
            {
                result = GetSubNodes(subNode);
                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            return result;
        }

        
        public ControllerModelTreeFactory(List<clsOntologyItem> viewModelItems, 
            List<clsObjectRel> viewModelsToDevelopments,
            List<clsObjectRel> projectsToDevelopments,
            ImageItem rootImageItem,
            ImageItem projectImageItem,
            ImageItem developmentImageItem,
            ImageItem viewModelImageItem,
            clsLocalConfig localConfig)
        {
            this.controllerModelItems = viewModelItems;
            this.controllerModelsToDevelopments = viewModelsToDevelopments;
            this.projectsToDevelopments = projectsToDevelopments;

            this.localConfig = localConfig;

            this.rootImageItem = rootImageItem;
            this.projectImageItem = projectImageItem;
            this.developmentImageItem = developmentImageItem;
            this.controllerModelImageItem = viewModelImageItem;

        }

        public clsOntologyItem WriteXMLDoc(StreamWriter streamWriter)
        {
            try
            {
                bool success = true;
                using (var xmlWriter = XmlWriter.Create(streamWriter))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("tree");
                    xmlWriter.WriteAttributeString("id", "0");
                    xmlWriter.WriteAttributeString("radio", "0");
                    success = nodeList.First().WriteXMLDoc(xmlWriter);
                    xmlWriter.WriteEndElement();
                }
                if (success)
                {
                    return localConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    return localConfig.Globals.LState_Error.Clone();
                }
                
            }
            catch (Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

        }

        private XmlDocument GetXMLDoc()
        {
            var xmlDoc = new XmlDocument();
            var rootNode = xmlDoc.CreateElement("tree");
            var xmlAttributeNode1 = (XmlAttribute)xmlDoc.CreateNode(XmlNodeType.Attribute, "id", "");
            xmlAttributeNode1.Value = "0";
            rootNode.Attributes.Append(xmlAttributeNode1);
            var xmlAttributeNode2 = (XmlAttribute)xmlDoc.CreateNode(XmlNodeType.Attribute, "radio", "");
            xmlAttributeNode2.Value = "0";
            rootNode.Attributes.Append(xmlAttributeNode1);



            var xmlNode = nodeList.First().GetXMLForDHTMLxControl(xmlDoc);

            rootNode.AppendChild(xmlNode);
            xmlDoc.AppendChild(rootNode);

            return xmlDoc;
        }

        public string GetBase64String()
        {
            return Base64Encode("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + GetXMLDoc().OuterXml);
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
