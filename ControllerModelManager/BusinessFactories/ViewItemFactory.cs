﻿using ControllerModelManager.Attributes;
using ControllerModelManager.BusinessModels;
using ControllerModelManager.Translations;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ControllerModelManager.BusinessFactories
{
    public class ViewItemFactory
    {
        private clsLocalConfig localConfig;
        private TranslationController translationController;

        private List<clsObjectRel> modelItemsOfModel;
        private List<clsObjectRel> modelItemTypesOfModelItems;
        private List<clsObjectRel> dataTypesOfModelItemTypes;
        private List<clsObjectRel> notificationsOfModelItems;

        private List<ViewItem> viewItems;

        public List<ViewItem> GetViewItems(List<clsObjectRel> modelItemsOfModel,
            List<clsObjectRel> modelItemTypesOfModelItems,
            List<clsObjectRel> dataTypesOfModelItemTypes,
            List<clsObjectRel> notificationsOfModelItems)
        {
            this.modelItemsOfModel = modelItemsOfModel;
            this.modelItemTypesOfModelItems = modelItemTypesOfModelItems;
            this.dataTypesOfModelItemTypes = dataTypesOfModelItemTypes;
            this.notificationsOfModelItems = notificationsOfModelItems;

            long rowId = 1;
            viewItems = (from modelItem in modelItemsOfModel
                             join modelItemType in modelItemTypesOfModelItems on modelItem.ID_Other equals modelItemType.ID_Object
                             join dataType in dataTypesOfModelItemTypes on modelItemType.ID_Other equals dataType.ID_Object
                             join notification in notificationsOfModelItems on modelItem.ID_Other equals notification.ID_Object
                             select new ViewItem
                             {
                                 Id = modelItem.ID_Other,
                                 Name = modelItem.Name_Other,
                                 IdModelItemType = modelItemType.ID_Other,
                                 NameModelItemType = modelItemType.Name_Other,
                                 IdClass = dataType.ID_Other,
                                 NameClass = dataType.Name_Other,
                                 IdNotification = notification.ID_Other,
                                 NameNotification = notification.Name_Other,
                                 OrderId = modelItem.OrderID != null ? modelItem.OrderID.Value : 1,
                                 RowId = rowId++
                             }).ToList();

            return viewItems;

        }


        public string GetBase64String()
        {
            return Base64Encode("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" + GetXMLDoc().OuterXml);
        }

        private string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public clsOntologyItem WriteXMLDoc(StreamWriter streamWriter)
        {
            try
            {
                using (var xmlWriter = XmlWriter.Create(streamWriter))
                {
                    xmlWriter.WriteStartDocument();
                    xmlWriter.WriteStartElement("rows");
                    viewItems.ForEach(viewItem =>
                    {
                        viewItem.WriteXMLDoc(xmlWriter);
                    });
                    xmlWriter.WriteEndElement();
                }
                return localConfig.Globals.LState_Success.Clone();
            }
            catch(Exception ex)
            {
                return localConfig.Globals.LState_Error.Clone();
            }
            
        }

        private XmlDocument GetXMLDoc()
        {
            var xmlDoc = new XmlDocument();

            var xmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "rows","");

            

            viewItems.ForEach(viewItem =>
            {
                xmlNode.AppendChild(viewItem.GetXMLForDHTMLxControl(xmlDoc));
            });

            xmlDoc.AppendChild(xmlNode);
            return xmlDoc;
        }

        public string GetHeaders()
        {
            return GetAttributeString("Header");

        }

        public List<int> GetHidden()
        {
            var colId = -1;
            var properties = typeof(ViewItem).GetProperties().Cast<PropertyInfo>().ToList();
            var attributes = properties.Select(propItem =>
            {
                var attribute = propItem.GetCustomAttribute<DhtmlXGridAttribute>();
                if (attribute == null) return null;
                colId++;
                if (!attribute.Hidden) return null;

                var dict = new Dictionary<string, string>();


                return new { OrderId = attribute.OrderId, ColId = colId };
            }).Where(attributeItem => attributeItem != null).OrderBy(attributeItem => attributeItem.OrderId).ToList();

            var hiddenIds = attributes.Select(attibItem => attibItem.ColId).ToList();

            return hiddenIds;
        }

        public string GetInitWiths()
        {
            return GetAttributeString("InitWith");

        }

        public string GetColSorts()
        {
            return GetAttributeString("ColSort");

        }

        public string GetColAligns()
        {
            return GetAttributeString("ColAlign");

        }

        public string GetColTypes()
        {
            return GetAttributeString("ColType");

        }

        public string GetFilters()
        {
            return GetAttributeString("Filter");
        }

        public string GetAttributeString(string attributeName)
        {
            var properties = typeof(ViewItem).GetProperties().Cast<PropertyInfo>().ToList();
            var attributes = properties.Select(propItem =>
            {
                var attribute = propItem.GetCustomAttribute<DhtmlXGridAttribute>();
                if (attribute == null) return null;
                
                var dict = new Dictionary<string, string>();

                var header = translationController.GetTranslatedByPropertyName(attribute.Header);

                dict.Add("Header", header);
                dict.Add("ColSort", attribute.ColSort);
                dict.Add("InitWith", attribute.InitWith);
                dict.Add("ColAlign", attribute.ColAlign);
                dict.Add("ColType", attribute.ColType);
                dict.Add("Filter", attribute.Filter);

                return new { OrderId = attribute.OrderId, DictAttribute = dict };
            }).Where(attributeItem => attributeItem != null && attributeItem.DictAttribute.ContainsKey(attributeName)).OrderBy(attributeItem => attributeItem.OrderId).ToList();

            if (attributes.Any())
            {
                return string.Join(",", attributes.Select(attributeItem => attributeItem.DictAttribute[attributeName].ToString()));
            }
            else
            {
                return null;
            }
            
        }

     

        public ViewItemFactory(clsLocalConfig localConfig, TranslationController translationController)
        {
            this.localConfig = localConfig;
            this.translationController = translationController;

        }



    }
}
