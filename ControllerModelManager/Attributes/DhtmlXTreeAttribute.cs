﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllerModelManager.Attributes
{
    class DhtmlXTreeAttribute : Attribute
    {
        public string DataAttribute { get; set; }
        public int OrderId { get; set; }
    }
}
