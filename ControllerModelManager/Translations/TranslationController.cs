﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ControllerModelManager.Translations
{
    public class TranslationController
    {

        public string Title_ImageListLoadError
        {
            get
            {
                return GetValue("16cff112d7e44e45804346679b5a3d3f", "Lade-Fehler");
            }

        }

        public string Text_ImageListLoadError
        {
            get
            {
                return GetValue("b8b0360e3cd5432aac235396f6aaf1f7", "Beim Laden der Image-Liste ist ein Fehler aufgetreten!");
            }

        }

        public string Title_ControllerModelTreeLoadError
        {
            get
            {
                return GetValue("ad38773bbc4c4c63a9dc7be0bd9fc214", "Lade-Fehler");
            }

        }

        public string Text_ControllerModelTreeLoadError
        {
            get
            {
                return GetValue("0f7a8cc50ce34ba182a85a68d42d0725", "Der Baum der Controller-Items konnte nicht geladen werden!");
            }

        }

        public string Title_ImageListTooShortError
        {
            get
            {
                return GetValue("68f78a8826c34df58d97221562b3238a", "Die Liste der Images ist nicht vollständig!");
            }

        }

        public string Text_ImageListTooShortError
        {
            get
            {
                return GetValue("f3d12aa3bb3343a184732eb02b68da09", "Liste unvollständig");
            }

        }

        public string DhtmlXCol_Name
        {
            get
            {
                return GetValue("938f73c58c8341f1bd1b6fa402cc932c", "Model-Element");
            }

        }

        public string DhtmlXCol_NameModelItemType
        {
            get
            {
                return GetValue("7a4ada6c3c1d459e9cc8b13cea727ef8", "Element-Typ");
            }

        }

        public string DhtmlXCol_NameNotification
        {
            get
            {
                return GetValue("5db8f8ae13ab48d1a5f74dfc99ef4eb2", "Notification-Element");
            }

        }

        public string DhtmlXCol_NameClass
        {
            get
            {
                return GetValue("fa4fda4cfc1a4bc0ad7bda87484bddf0", "C#-Klasse");
            }

        }

        public string DhtmlXCol_OrderId
        {
            get
            {
                return GetValue("7059d8cd854e4ffaa5565ae1c614fa4a", "Order-Id");
            }

        }

        public string Title_ModelItemLoadError
        {
            get
            {
                return GetValue("fb7c6f75a9b94a8bb0aeb2b8484027ca", "Lade-Fehler");
            }

        }

        public string Text_ModelItemLoadError
        {
            get
            {
                return GetValue("d675814687924abdb0255217f87e18dc", "Beim Laden der Model-Elemente ist ein Fehler aufgetreten!");
            }

        }

        public string Title_ParamMissingError
        {
            get
            {
                return GetValue("8682b6cc88eb43c199204362a6593a61", "Parameter fehlt!");
            }

        }

        public string Text_ParamMissingError
        {
            get
            {
                return GetValue("78f1930d728b48879655157200a1c37e", "Der Parameter {0} wurde nicht übermittelt!");
            }

        }

        public string Title_NodeNotPresentError
        {
            get
            {
                return GetValue("a66af0e4c2d747faaafa07938c3fff79", "Knoten nicht gefunden!");
            }

        }

        public string Text_NodeNotPresentError
        {
            get
            {
                return GetValue("91362775be824af39053ded0d845c37a", "Der Knoten konnte nicht gefunden werden!");
            }

        }

        public string Title_FileStreamError
        {
            get
            {
                return GetValue("244d7e5cd2e64401bd99a91ba98fda34", "Datei-Fehler!");
            }

        }

        public string Text_FileStreamError
        {
            get
            {
                return GetValue("5682b2ddf0be4d1babeac59245d25a85", "Datei für Datenaustausch konnte  nicht agespeichert werden!");
            }

        }

        public string Title_WebConfigError
        {
            get
            {
                return GetValue("78634024801646ceba093bb8c62f7f0a", "Webserver-Fehler");
            }

        }

        public string Text_WebConfigError
        {
            get
            {
                return GetValue("7d1199b177ba435b8c2bcb917cec913b", "Der Webserver hat einen Fehler verursacht!");
            }

        }

        public string Title_ConnectionLost
        {
            get
            {
                return GetValue("5edd1584059a4e80b91a318171960027", "Verbindungsproblem!");
            }

        }

        public string Text_ViewItemTree
        {
            get
            {
                return GetValue("5fc478f46d8b4a65896cd93e84cb2eca", "Model-Element Baum");
            }

        }

        public string Text_Details
        {
            get
            {
                return GetValue("230359ca1bfb4530b3e47ff3bf171322", "Detailanischt");
            }

        }

        public string Text_ConnectionLost
        {
            get
            {
                return GetValue("1512a5da63594b8cae7997bcb1930a95", "Die Verbindung wurde unterbrochen. Sie können versuchen, die Verbindung erneut aufzubauen.");
            }

        }

        public string Text_ModelItemList
        {
            get
            {
                return GetValue("fe9b706642054d699512d821e0a83910", "Liste d. Model-Elemente");
            }

        }

        public string Text_TreeNodeCreation
        {
            get
            {
                return GetValue("d85f0c38498f41959bba40e694898838", "Baumelement-Bearbeitung");
            }

        }

        public string Text_ModelItemCreation
        {
            get
            {
                return GetValue("dbd0cd510a2e452fb9a8504390fccd6d", "Model-Element Bearbeitung");
            }

        }

        public string Label_ItemType
        {
            get
            {
                return GetValue("b5c3f58ca1b44e1d88aa8bda0299401b", "Element-Typ:");
            }

        }

        public string Label_ItemName
        {
            get
            {
                return GetValue("7d33fc06dcea48a59d48e0a3b7c17934", "Element-Name:");
            }

        }


        public string GetTranslatedByPropertyName(string propertyName)
        {
            var property = this.GetType().GetProperties().Cast<PropertyInfo>().FirstOrDefault(propItem => propItem.Name == propertyName);

            if (property != null)
            {
                var value = property.GetValue(this);
                return value != null ? value.ToString() : "xxx_Error_xxx";
            }
            else
            {
                return "xxx_Error_xxx";
            }
        }


        private string GetValue(string IdReference, string defaultValue)
        {
            return defaultValue;
        }

    }
}
