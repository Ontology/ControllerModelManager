﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Runtime.InteropServices;
using OntologyClasses.Interfaces;

namespace ControllerModelManager
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "d5b73edac0bf42f294c1482f0623f26c";
        
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        // Classes
        public clsOntologyItem OItem_class_datatypes__programing_languages_ { get; set; }
        public clsOntologyItem OItem_class_notification { get; set; }
        public clsOntologyItem OItem_class_path { get; set; }
        public clsOntologyItem OItem_class_software_development { get; set; }
        public clsOntologyItem OItem_class_software_project { get; set; }
        public clsOntologyItem OItem_class_view_items { get; set; }
        public clsOntologyItem OItem_class_view_model { get; set; }
        public clsOntologyItem OItem_class_view_property_type { get; set; }

        // Objects
        public clsOntologyItem OItem_object_datatext { get; set; }
        public clsOntologyItem OItem_object_imageid { get; set; }
        public clsOntologyItem OItem_object_ischecked { get; set; }
        public clsOntologyItem OItem_object_isenabled { get; set; }
        public clsOntologyItem OItem_object_isvisble { get; set; }
        public clsOntologyItem OItem_object_text { get; set; }
        public clsOntologyItem OItem_object_ControllerModelManager { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_belongs_to { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_image_path { get; set; }
        public clsOntologyItem OItem_relationtype_is_of_type { get; set; }
        public clsOntologyItem OItem_relationtype_uses { get; set; }



        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();

                objDBLevel_Config1 = null;
                objDBLevel_Config2 = null;
            }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {

    }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_belongs_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belongs_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_belongs_to.Any())
            {
                OItem_relationtype_belongs_to = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongs_to.First().ID_Other,
                    Name = objOList_relationtype_belongs_to.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongs_to.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_image_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_image_path".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_image_path.Any())
            {
                OItem_relationtype_image_path = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_image_path.First().ID_Other,
                    Name = objOList_relationtype_image_path.First().Name_Other,
                    GUID_Parent = objOList_relationtype_image_path.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_of_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_of_type".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_is_of_type.Any())
            {
                OItem_relationtype_is_of_type = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_of_type.First().ID_Other,
                    Name = objOList_relationtype_is_of_type.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_of_type.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_uses = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_uses".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_uses.Any())
            {
                OItem_relationtype_uses = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_uses.First().ID_Other,
                    Name = objOList_relationtype_uses.First().Name_Other,
                    GUID_Parent = objOList_relationtype_uses.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

    private void get_Config_Objects()
    {
            var objOList_object_datatext = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_datatext".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

            if (objOList_object_datatext.Any())
            {
                OItem_object_datatext = new clsOntologyItem()
                {
                    GUID = objOList_object_datatext.First().ID_Other,
                    Name = objOList_object_datatext.First().Name_Other,
                    GUID_Parent = objOList_object_datatext.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_imageid = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "object_imageid".ToLower() && objRef.Ontology == Globals.Type_Object
                                           select objRef).ToList();

            if (objOList_object_imageid.Any())
            {
                OItem_object_imageid = new clsOntologyItem()
                {
                    GUID = objOList_object_imageid.First().ID_Other,
                    Name = objOList_object_imageid.First().Name_Other,
                    GUID_Parent = objOList_object_imageid.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_ischecked = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "object_ischecked".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

            if (objOList_object_ischecked.Any())
            {
                OItem_object_ischecked = new clsOntologyItem()
                {
                    GUID = objOList_object_ischecked.First().ID_Other,
                    Name = objOList_object_ischecked.First().Name_Other,
                    GUID_Parent = objOList_object_ischecked.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_isenabled = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "object_isenabled".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

            if (objOList_object_isenabled.Any())
            {
                OItem_object_isenabled = new clsOntologyItem()
                {
                    GUID = objOList_object_isenabled.First().ID_Other,
                    Name = objOList_object_isenabled.First().Name_Other,
                    GUID_Parent = objOList_object_isenabled.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_isvisble = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "object_isvisble".ToLower() && objRef.Ontology == Globals.Type_Object
                                            select objRef).ToList();

            if (objOList_object_isvisble.Any())
            {
                OItem_object_isvisble = new clsOntologyItem()
                {
                    GUID = objOList_object_isvisble.First().ID_Other,
                    Name = objOList_object_isvisble.First().Name_Other,
                    GUID_Parent = objOList_object_isvisble.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_text = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "object_text".ToLower() && objRef.Ontology == Globals.Type_Object
                                        select objRef).ToList();

            if (objOList_object_text.Any())
            {
                OItem_object_text = new clsOntologyItem()
                {
                    GUID = objOList_object_text.First().ID_Other,
                    Name = objOList_object_text.First().Name_Other,
                    GUID_Parent = objOList_object_text.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_ControllerModelManager = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "object_ControllerModelManager".ToLower() && objRef.Ontology == Globals.Type_Object
                                                       select objRef).ToList();

            if (objOList_object_ControllerModelManager.Any())
            {
                OItem_object_ControllerModelManager = new clsOntologyItem()
                {
                    GUID = objOList_object_ControllerModelManager.First().ID_Other,
                    Name = objOList_object_ControllerModelManager.First().Name_Other,
                    GUID_Parent = objOList_object_ControllerModelManager.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

    private void get_Config_Classes()
    {
            var objOList_class_datatypes__programing_languages_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                   where objOItem.ID_Object == cstrID_Ontology
                                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                   where objRef.Name_Object.ToLower() == "class_datatypes__programing_languages_".ToLower() && objRef.Ontology == Globals.Type_Class
                                                                   select objRef).ToList();

            if (objOList_class_datatypes__programing_languages_.Any())
            {
                OItem_class_datatypes__programing_languages_ = new clsOntologyItem()
                {
                    GUID = objOList_class_datatypes__programing_languages_.First().ID_Other,
                    Name = objOList_class_datatypes__programing_languages_.First().Name_Other,
                    GUID_Parent = objOList_class_datatypes__programing_languages_.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_notification = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "class_notification".ToLower() && objRef.Ontology == Globals.Type_Class
                                               select objRef).ToList();

            if (objOList_class_notification.Any())
            {
                OItem_class_notification = new clsOntologyItem()
                {
                    GUID = objOList_class_notification.First().ID_Other,
                    Name = objOList_class_notification.First().Name_Other,
                    GUID_Parent = objOList_class_notification.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_path = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_path".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_class_path.Any())
            {
                OItem_class_path = new clsOntologyItem()
                {
                    GUID = objOList_class_path.First().ID_Other,
                    Name = objOList_class_path.First().Name_Other,
                    GUID_Parent = objOList_class_path.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_software_development = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "class_software_development".ToLower() && objRef.Ontology == Globals.Type_Class
                                                       select objRef).ToList();

            if (objOList_class_software_development.Any())
            {
                OItem_class_software_development = new clsOntologyItem()
                {
                    GUID = objOList_class_software_development.First().ID_Other,
                    Name = objOList_class_software_development.First().Name_Other,
                    GUID_Parent = objOList_class_software_development.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_software_project = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "class_software_project".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

            if (objOList_class_software_project.Any())
            {
                OItem_class_software_project = new clsOntologyItem()
                {
                    GUID = objOList_class_software_project.First().ID_Other,
                    Name = objOList_class_software_project.First().Name_Other,
                    GUID_Parent = objOList_class_software_project.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_view_items = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_view_items".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_class_view_items.Any())
            {
                OItem_class_view_items = new clsOntologyItem()
                {
                    GUID = objOList_class_view_items.First().ID_Other,
                    Name = objOList_class_view_items.First().Name_Other,
                    GUID_Parent = objOList_class_view_items.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_view_model = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_view_model".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_class_view_model.Any())
            {
                OItem_class_view_model = new clsOntologyItem()
                {
                    GUID = objOList_class_view_model.First().ID_Other,
                    Name = objOList_class_view_model.First().Name_Other,
                    GUID_Parent = objOList_class_view_model.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_view_property_type = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "class_view_property_type".ToLower() && objRef.Ontology == Globals.Type_Class
                                                     select objRef).ToList();

            if (objOList_class_view_property_type.Any())
            {
                OItem_class_view_property_type = new clsOntologyItem()
                {
                    GUID = objOList_class_view_property_type.First().ID_Other,
                    Name = objOList_class_view_property_type.First().Name_Other,
                    GUID_Parent = objOList_class_view_property_type.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}