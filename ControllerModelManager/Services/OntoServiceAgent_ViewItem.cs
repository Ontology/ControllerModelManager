﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllerModelManager.Services
{
    public class OntoServiceAgent_ViewItem : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector ontoServive_ModelItems;
        private OntologyModDBConnector ontoService_ModelItemTypes;
        private OntologyModDBConnector ontoService_ClassTypes;
        private OntologyModDBConnector ontoService_Notification;

        public List<clsObjectRel> ModelItems
        {
            get
            {
                return ontoServive_ModelItems.ObjectRels;
            }
            
        }

        public List<clsObjectRel> ModelItemTypesOfModelItems
        {
            get
            {
                return ontoService_ModelItemTypes.ObjectRels;
            }
        }

        public List<clsObjectRel> DataTypesOfModelItemTypes
        {
            get
            {
                return ontoService_ClassTypes.ObjectRels;
            }
        }

        public List<clsObjectRel> NotificationsOfModelItems
        {
            get
            {
                return ontoService_Notification.ObjectRels;
            }
        }

        public OntoServiceAgent_ViewItem(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public clsOntologyItem GetDataViewItems(string idModel)
        {
            var result = GetSubData001_ViewItemsOfViewModel(idModel);

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = GetSubData002_ViewItemTypes();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = GetSubData003_ClassTypesOfViewItemTypes();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = GetSubData004_Notifications();

            return result;

        }

        private clsOntologyItem GetSubData001_ViewItemsOfViewModel(string idModel)
        {
            var searchModelItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = idModel,
                    ID_Parent_Object = localConfig.OItem_class_view_model.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.OItem_class_view_items.GUID
                }
            };

            var result = ontoServive_ModelItems.GetDataObjectRel(searchModelItems);

            return result;
        }

        private clsOntologyItem GetSubData002_ViewItemTypes()
        {
            ontoService_ModelItemTypes.ObjectRels.Clear();
            var searchModelItemTypes = ontoServive_ModelItems.ObjectRels.Select(viewModel => new clsObjectRel
            {
                ID_Object = viewModel.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                ID_Parent_Other = localConfig.OItem_class_view_property_type.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchModelItemTypes.Any())
            {
                result = ontoService_ModelItemTypes.GetDataObjectRel(searchModelItemTypes);
            }

            return result;
        }

        private clsOntologyItem GetSubData003_ClassTypesOfViewItemTypes()
        {
            ontoService_ClassTypes.ObjectRels.Clear();

            var searchClassTypesOfViewItemTypes = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_class_view_property_type.GUID,
                    ID_Parent_Other = localConfig.OItem_class_datatypes__programing_languages_.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID
                }
            };

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchClassTypesOfViewItemTypes.Any())
            {
                result = ontoService_ClassTypes.GetDataObjectRel(searchClassTypesOfViewItemTypes);
            }

            return result;
        }

        private clsOntologyItem GetSubData004_Notifications()
        {
            ontoService_Notification.ObjectRels.Clear();
            var searchNotifications = ontoServive_ModelItems.ObjectRels.Select(viewModel => new clsObjectRel
            {
                ID_Object = viewModel.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_uses.GUID,
                ID_Parent_Other = localConfig.OItem_class_notification.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchNotifications.Any())
            {
                result = ontoService_Notification.GetDataObjectRel(searchNotifications);
            }

            return result;
        }

        private void Initialize()
        {
            ontoService_ClassTypes = new OntologyModDBConnector(localConfig.Globals);
            ontoService_ModelItemTypes = new OntologyModDBConnector(localConfig.Globals);
            ontoService_Notification = new OntologyModDBConnector(localConfig.Globals);
            ontoServive_ModelItems = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
