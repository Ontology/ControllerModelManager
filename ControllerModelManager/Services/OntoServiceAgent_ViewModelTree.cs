﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllerModelManager.Services
{
    public class OntoServiceAgent_ViewModelTree : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector ontoServive_SoftwareProject;
        private OntologyModDBConnector ontoService_Development;
        private OntologyModDBConnector ontoService_ViewModel;

        public List<clsOntologyItem> ViewModelList
        {
            get
            {
                return ontoService_ViewModel.Objects1;
            }
            
        }

        public List<clsObjectRel> ViewModelsToDevelopments
        {
            get
            {
                return ontoService_Development.ObjectRels;
            }
        }

        public List<clsObjectRel> ProjectsToDevelopments
        {
            get
            {
                return ontoServive_SoftwareProject.ObjectRels;
            }
        }
        
        public OntoServiceAgent_ViewModelTree(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        public clsOntologyItem GetDataViewModelTree()
        {
            var result = GetSubData001_ViewModels();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = GetSubData002_Developments();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = GetSubData003_Projects();

            return result;

        }

        private clsOntologyItem GetSubData001_ViewModels()
        {
            var searchViewModels = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_class_view_model.GUID
                }
            };

            var result = ontoService_ViewModel.GetDataObjects(searchViewModels);

            return result;
        }

        private clsOntologyItem GetSubData002_Developments()
        {
            ontoService_Development.ObjectRels.Clear();
            var searchDevelopments = ontoService_ViewModel.Objects1.Select(viewModel => new clsObjectRel
            {
                ID_Object = viewModel.GUID,
                ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Other = localConfig.OItem_class_software_development.GUID
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchDevelopments.Any())
            {
                result = ontoService_Development.GetDataObjectRel(searchDevelopments);
            }

            return result;
        }

        private clsOntologyItem GetSubData003_Projects()
        {
            ontoServive_SoftwareProject.ObjectRels.Clear();

            var searchProjects = ontoService_Development.ObjectRels.Select(development => new clsObjectRel
            {
                ID_Parent_Object = localConfig.OItem_class_software_project.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Other = development.ID_Other
            }).ToList();

            var result = localConfig.Globals.LState_Success.Clone();
            if (searchProjects.Any())
            {
                result = ontoServive_SoftwareProject.GetDataObjectRel(searchProjects);
            }

            return result;
        }

        private void Initialize()
        {
            ontoService_Development = new OntologyModDBConnector(localConfig.Globals);
            ontoService_ViewModel = new OntologyModDBConnector(localConfig.Globals);
            ontoServive_SoftwareProject = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
