﻿using ControllerModelManager.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ControllerModelManager.BusinessModels
{
    public class ControllerModelTreeNode
    {
        [DhtmlXTree(DataAttribute ="id", OrderId = 0)]
        public string Id { get; set; }

        [DhtmlXTree(DataAttribute = "text", OrderId = 1)]
        public string Name { get; set; }

        public List<ControllerModelTreeNode> SubNodes { get; set; }
        public ControllerModelTreeNode ParentNode { get; set; }

        public string IdNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im2", OrderId = 5)]
        public string NameNodeImage { get; set; }

        //[DhtmlXTree(DataAttribute = "im1", OrderId = 4)]
        public string NameNodeImageSelected
        {
            get { return NameNodeImage; }
        }

        [DhtmlXTree(DataAttribute = "im0", OrderId = 3)]
        public string NameNodeImageNoChildren
        {
            get { return NameNodeImage + ".png"; }
        }

        public string IdPath { get; set; }

        [DhtmlXTree(DataAttribute = "child", OrderId = 2)]
        public string HasSubNodes
        {
            get
            {
                if (SubNodes != null && SubNodes.Any())
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
        }

        public bool WriteXMLDoc(XmlWriter xmlWriter)
        {
            try
            {
                xmlWriter.WriteStartElement("item");
                this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
                {
                    var attribute = propItem.GetCustomAttribute<DhtmlXTreeAttribute>();

                    if (attribute != null)
                    {
                        xmlWriter.WriteAttributeString(attribute.DataAttribute, System.Web.HttpUtility.HtmlEncode(propItem.GetValue(this).ToString()));
                        
                    }


                });

                if (SubNodes != null && SubNodes.Any())
                {
                    SubNodes.ForEach(subNode =>
                    {
                        subNode.WriteXMLDoc(xmlWriter);
                    });

                }

                xmlWriter.WriteEndElement();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public XmlNode GetXMLForDHTMLxControl(XmlDocument xmlDocument)
        {

            
            var xmlNode = xmlDocument.CreateNode(XmlNodeType.Element, "item", "");

            this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
            {


                var attribute = propItem.GetCustomAttribute<DhtmlXTreeAttribute>();

                if (attribute != null)
                {

                    var xmlAttributeNode = (XmlAttribute)xmlDocument.CreateNode(XmlNodeType.Attribute, attribute.DataAttribute, "");
                    xmlAttributeNode.Value = System.Web.HttpUtility.HtmlEncode(propItem.GetValue(this).ToString());
                    xmlNode.Attributes.Append(xmlAttributeNode);
                }


            });

            

            if (SubNodes != null && SubNodes.Any())
            {
                SubNodes.ForEach(subNode =>
                {
                    xmlNode.AppendChild(subNode.GetXMLForDHTMLxControl(xmlDocument));
                });
                
            }
            
            return xmlNode;
        }

    }
}
