﻿using ControllerModelManager.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ControllerModelManager.BusinessModels
{
    public class ViewItem
    {
        [DhtmlXGrid(
            Header = "DhtmlXCol_Id", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 0, 
            Hidden = true)]
        public string Id { get; set; }

        [DhtmlXGrid(
            Header = "DhtmlXCol_Name", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100", 
            Filter = "#text_filter",
            OrderId = 1)]
        public string Name { get; set; }

        [DhtmlXGrid(
            Header = "DhtmlXCol_IdModelItemType", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 2, 
            Hidden = true)]
        public string IdModelItemType { get; set; }

        [DhtmlXGrid(Header = "DhtmlXCol_NameModelItemType", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 3)]
        public string NameModelItemType { get; set; }

        [DhtmlXGrid(
            Header = "DhtmlXCol_IdNotification", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 4, 
            Hidden = true)]
        public string IdNotification { get; set; }

        [DhtmlXGrid(Header = "DhtmlXCol_NameNotification", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter", 
            OrderId = 5)]
        public string NameNotification { get; set; }

        [DhtmlXGrid(Header = "DhtmlXCol_IdClass", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 6, 
            Hidden = true)]
        public string IdClass { get; set; }

        [DhtmlXGrid(Header = "DhtmlXCol_NameClass", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "str", 
            InitWith = "100",
            Filter = "#text_filter",
            OrderId = 7)]
        public string NameClass { get; set; }

        [DhtmlXGrid(Header = "DhtmlXCol_OrderId", 
            ColAlign = "left", 
            ColType = "ro", 
            ColSort = "int", 
            InitWith = "100",
            Filter = "#numeric_filter",
            OrderId = 8)]
        public long OrderId { get; set; }

        public long RowId { get; set; }

        public bool WriteXMLDoc(XmlWriter xmlWriter)
        {
            try
            {
                xmlWriter.WriteStartElement("row");
                xmlWriter.WriteAttributeString("id", RowId.ToString());
                this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
                {
                    xmlWriter.WriteStartElement("cell");
                    xmlWriter.WriteCData(System.Web.HttpUtility.HtmlEncode(propItem.GetValue(this).ToString()));
                    xmlWriter.WriteEndElement();
                });
                xmlWriter.WriteEndElement();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            
        }
        public XmlNode GetXMLForDHTMLxControl(XmlDocument xmlDocument)
        {


            var xmlNode = xmlDocument.CreateNode(XmlNodeType.Element, "row", "");

            var xmlAttributeNode = (XmlAttribute)xmlDocument.CreateNode(XmlNodeType.Attribute, "id", "");
            xmlAttributeNode.Value = RowId.ToString();

            xmlNode.Attributes.Append(xmlAttributeNode);

            this.GetType().GetProperties().Cast<PropertyInfo>().ToList().ForEach(propItem =>
            {
                var cellNode = xmlDocument.CreateNode(XmlNodeType.Element, "cell", "");
                var cellValueNode = xmlDocument.CreateNode(XmlNodeType.CDATA, "", "");
                cellValueNode.Value = System.Web.HttpUtility.HtmlEncode(propItem.GetValue(this).ToString());
                cellNode.AppendChild(cellValueNode);
                xmlNode.AppendChild(cellNode);
            });

            return xmlNode;
        }

    }

}
